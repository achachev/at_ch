// Setting up Webpack to watch ES6 tranpsiling /w babel

var path = require('path'),
		webpack = require('webpack');

module.exports = {
	entry: './src/js/main.js',
	output: {
		path: path.resolve(__dirname , 'build/js'),
		filename: './main.bundle.js'
	},
	module: {
		loaders: [
			{
			 test: /\.js$/,
	     loader: 'babel-loader',
	     query: {
	         presets: ['es2015']
	     }
			}
		]
	},
	stats: {
		colors: true
	},
	watch: true,
	devtool: 'source-map'
};