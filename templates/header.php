<!doctype html>
<html>
	<head>
		<title>AT_CH App</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- icons font -->
		<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

		<!-- fonts -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

		<!-- styles -->
		<link rel="stylesheet" href="build/styles/styles.css">
	</head>
<body>

<div id="wrapper">
	<!-- header -->
	<header class="header page__header">
		<div class="cover--container" id="cover--photo-holder">
			<div class="logout-btn text-right">
				<button class="button--state-cancel bold" type="button">Log out</button>
			</div>

			<div class="user--coverphoto-upload text-center">
				<label class="upload--cover-btn" for="coverPhoto">Upload cover image</label>
				<input type="file" id="coverPhoto">
			</div>
			
			<!-- Header user image + additional info -->
			<div class="user--profile-container">
				<div id="user--profile-image"></div>

				<div class="user--profile-info">
					<strong class="user--profile-name" id="profile--user-name"></strong>
					<span class="user--profile-city" id="profile--user-city"></span>
					<span class="user--profile-phone" id="profile--user-phone"></span>
				</div>

				<div class="user--profile-rate">
					<span class="rate--star"></span>
					<span class="rate--star"></span>
					<span class="rate--star"></span>
					<span class="rate--star"></span>
					<span class="rate--star"></span>
					
					<div class="review--number">
						<span>6</span>	
						<span>Reviews</span>
					</div>
				</div>

				<div class="user--followers-info">
					<span>15</span>	
					<span>Followers</span>
				</div>
			</div>
		</div>
		<!-- /Header user image + additional info -->

		<!-- tabs navigation -->
		<nav class="page__navigation">
			<ul class="menu">
				<li>
					<button class="menu__list-item" type="button">About</button>
				</li>
				<li>
					<button class="menu__list-item" type="button">Settings</button>
				</li>
				<li>
					<button class="menu__list-item" type="button">Option 1</button>
				</li>
				<li>
					<button class="menu__list-item" type="button">Option 2</button>
				</li>
				<li>
					<button class="menu__list-item" type="button">Option 3</button>
				</li>
			</ul>
		</nav>
		<!-- /tabs-navigation -->
	</header>