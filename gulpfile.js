var gulp = require('gulp'),
		sass = require('gulp-sass'),
		sourcemaps = require('gulp-sourcemaps');


var sassOptions = {
	errorLogToConsole: true,
	outputStyle: 'compressed'
};

gulp.task('sass' , () => {
	return gulp.src('src/styles/**/*.scss')
	.pipe(sourcemaps.init())
	.pipe(sass(sassOptions).on('error', sass.logError))
	.pipe(sourcemaps.write('./'))
	.pipe(gulp.dest('build/styles'));
});

gulp.task('watch:sass' , () => {
	gulp.watch('src/styles/**/*.scss' , [sass]);
});