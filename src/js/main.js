import 'babel-polyfill';

let AT_CH ,
	AT_CH_CONTROLLER = function() {
		let _self = this,
		 		person = null,
		 		defaultInfo = {};

		const profileUserName = document.getElementById('profile--user-name');
		const	profileUserCity = document.getElementById('profile--user-city');
		const profileUserPhone = document.getElementById('profile--user-phone');

		this.init = () => {
			_self.getPersonInfo();
			_self.toggleTabsContent();
			_self.fillRateStars();
		}

		// Setting up the user data

		this.getPersonInfo = () => {
			let objectRequest = new XMLHttpRequest();
					objectRequest.open('GET' , './user.json');
					objectRequest.send();

			//Creating default object when there's nothing in the localStorage.

			objectRequest.onreadystatechange = () => {
				  if (objectRequest.readyState === XMLHttpRequest.DONE) {
				    if (objectRequest.status === 200) {
				      defaultInfo = JSON.parse(objectRequest.responseText);
				      person = JSON.parse(localStorage.getItem('person')) || defaultInfo;

				      _self.templateInit();
				      _self.mobileEdit();
				      _self.setProfileImage();
				      _self.setRating();
				      _self.changeCoverPhoto();
				    }
				  }
			};
		}


		// Set cover photo
		this.changeCoverPhoto = () => {
			let uploadFile = document.getElementById('coverPhoto');
			let coverPhotoContainer = document.getElementById('cover--photo-holder');

			coverPhotoContainer.style.backgroundImage = `url(${person.coverImage})`;

			uploadFile.addEventListener('change' , (e) => {
				let imageSrc = URL.createObjectURL(e.currentTarget.files[0]);
				coverPhotoContainer.style.backgroundImage = `url(${imageSrc})`;

				person.coverImage = imageSrc;
				_self.updateUserInfo();
			});
		}

		this.getAllPreviousSiblingsElements = (elem, filter) => {
			var siblings = [];

	    while (elem = elem.previousSibling) {
	        if (elem.nodeType === 3) continue;
	        if (!filter || filter(elem)) siblings.push(elem);
	    }

	    return siblings;
		}

		this.getAllNextSiblingElements = (elem , filter) => {
		  var siblings = [];

	    while (elem = elem.nextSibling) {
	        if (elem.nodeType === 3) continue;
	        if (!filter || filter(elem)) siblings.push(elem);
	    }

	    return siblings;
		}

		//Updating the userinfo in the local storage

		this.updateUserInfo = () => localStorage.setItem('person' , JSON.stringify(person));

		//Setting the user rating.

		this.setUserRating = () => {
			let rateNumber =  document.querySelectorAll('.rate--star.filled').length;

			person.rating = rateNumber;
			_self.updateUserInfo();
		}

		this.fillRateStars = () => {
			let stars = document.querySelectorAll('.user--profile-rate .rate--star');

			for(let star = 0 ; star < stars.length ; star++) {
				stars[star].addEventListener('mouseover' , (e) => {
					let _this = e.currentTarget;
					
					let previousElements = _self.getAllPreviousSiblingsElements(_this),
							nextElements = _self.getAllNextSiblingElements(_this);
					
					_this.classList.add('filled');

					for(let prevEl = 0 ; prevEl < previousElements.length ; prevEl++) {
						if(!previousElements[prevEl].classList.contains('filled')) {
							previousElements[prevEl].classList.add('filled');
						}
					}

					for(let nextEl = 0 ; nextEl < nextElements.length ; nextEl++) {
						if(nextElements[nextEl].classList.contains('filled')) {
							nextElements[nextEl].classList.remove('filled');
						}
					}
				});

				stars[star].addEventListener('mouseout', _self.setUserRating);
				stars[star].addEventListener('click' , _self.setUserRating);
			}
		}

		this.mobileEdit = () => {
			let mobileEditButton = document.querySelector('.mobile--edit'),
					mobileSaveButton = document.querySelector('.mobile--save'),
					mobileCancelButton = document.querySelector('.mobile--cancel');
			
			let personInfo = document.querySelectorAll('.person-info');
			let inputValues = document.querySelectorAll('.floating-placeholder');

			let isFormValid = true;

			if(mobileEditButton != null) {
				mobileEditButton.addEventListener('click' , (e) => {
					let _this = e.currentTarget;

					for(var _pi = 0 ; _pi < personInfo.length ; _pi++) {
						personInfo[_pi].lastElementChild.firstElementChild.classList.add('input-filled');
						personInfo[_pi].lastElementChild.firstElementChild.firstElementChild.value = personInfo[_pi].firstElementChild.firstElementChild.innerHTML.trim();

						personInfo[_pi].classList.add('active');
					}

					_this.parentElement.classList.add('active');
				});
			}

			if(mobileCancelButton != null) {
				mobileCancelButton.addEventListener('click' , (e) => {
					let _this = e.currentTarget;

					for(var mobileCancel = 0 ; mobileCancel < personInfo.length ; mobileCancel++) {
						personInfo[mobileCancel].classList.remove('active');
					}

					_this.parentElement.classList.remove('active');
				});
			}

			if(mobileSaveButton != null) {
				mobileSaveButton.addEventListener('click' , (e) => {
					let _this = e.currentTarget;

					for(var inpVal = 0 ; inpVal < inputValues.length ; inpVal++) {
						let propName = inputValues[inpVal].parentElement.parentElement.getAttribute('data-prop');
						let newPropValue = inputValues[inpVal].firstElementChild.value;
						let propNameTitle = inputValues[inpVal].parentElement.previousElementSibling.firstElementChild;

						if(newPropValue != '' && newPropValue.length > 0) {

							for(let key in person.data) {	
								let firstParam = Object.keys(person.data[key])[0];	

								if(firstParam == propName) {
									person.data[key][propName] = newPropValue;
								} 
							}

							_self.updateUserInfo();
							propNameTitle.innerHTML = newPropValue;
							_self.setHeaderInfoOnUpdate(propName, newPropValue);
							isFormValid = true;
						}	else {
							isFormValid = false;
							break;
						}
					}

					if(isFormValid) {
						for(var _pi = 0 ; _pi < personInfo.length ; _pi++) {
							personInfo[_pi].classList.remove('active');
						}		
						_this.parentElement.classList.remove('active');
					} else {
						alert('A field cannot be empty');
					}
				});
			}
		}

		this.setProfileImage = () => {
			let profileImageContainer = document.getElementById('user--profile-image');

			profileImageContainer.style.backgroundImage = `url(${person.image})`;
		}

		this.setRating = () => {
			let personRating = person.rating,
					ratingStars = document.querySelectorAll('.rate--star');

			for(var rate = 0 ; rate < ratingStars.length ; rate++) {
				if(rate < personRating) {
					ratingStars[rate].classList.add('filled');
				}
			}
		};

		//Creating the tabs functionallity

		this.toggleTabsContent = () => {
			let tabButtons = document.querySelectorAll('.menu__list-item');
			let tabContent = document.querySelectorAll('.tab-content');

			let tabButtonId = null;

			if(tabButtons.length && tabContent.length) {
				tabContent[0].classList.add('active');
				tabButtons[0].classList.add('active');
			}

			for(let i = 0 , len = tabButtons.length ; i < len ; i++) {

				tabButtons[i].setAttribute('data-tabid' , i);

				tabButtons[i].addEventListener('click' , (e) => {
					let _this = e.currentTarget;
					let activeButtonElement = document.querySelector('.menu__list-item.active');
					let activeTabContent = document.querySelector('.tab-content.active');

						tabButtonId = _this.getAttribute('data-tabid');
					
					activeButtonElement.classList.remove('active');
					_this.classList.add('active');

					activeTabContent.classList.remove('active');
					tabContent[tabButtonId].classList.add('active');
				});
			}
		}


		this.setHeaderInfo = () => {
			for(let key in person.data) {
				if(person.data[key].phone) profileUserPhone.innerHTML = person.data[key].phone;
				if(person.data[key].name) profileUserName.innerHTML = person.data[key].name;
				if(person.data[key].city) profileUserCity.innerHTML = person.data[key].city;
			}
		}

		this.setHeaderInfoOnUpdate = (propName, newPropValue) => {
			switch(propName) {
				case 'name' :
					profileUserName.innerHTML = newPropValue;
						break;

				case 'city' :
					profileUserCity.innerHTML = newPropValue;
						break;

				case 'phone' :
					profileUserPhone.innerHTML = newPropValue;
						break;

				default:
					break;
			}
		}

		//Initializing the template and creating the edit , save and cancel functionallity

		this.templateInit = () => {

			let mobileControlsTemplate = `
			 	<div class="mobile--controls">
			 		<button class="button--state-edit mobile--edit">Edit</button>
			 		<button class="button--state-save mobile--save">Save</button>
			 		<button class="button--state-cancel mobile--cancel">Cancel</button>
			 	</div>	
			`;

			let containerHolder = document.createElement('div');
					containerHolder.innerHTML = mobileControlsTemplate;

			let userInfoTemplateContainer = document.createElement('div');

			for(let data = 0 ; data < person.data.length ; data++) {
				let userInfoTemplate = document.createElement('div');

				let firstParam = Object.keys(person.data[data])[0],
						secondParam = Object.keys(person.data[data])[1];

				let firstParamValue = person.data[data][firstParam],
						secondParamValue = person.data[data][secondParam];

				userInfoTemplate.innerHTML = `
						<div class="person-info--container">
							<div class="person-info" data-prop="${ firstParam }">
								<div class="default-info">
									<span> ${ firstParamValue } </span> 
									<button class="edit button--state-edit" type="button"> Edit </button>
								</div>

								<div class="person-info-edit">
									
									<div class="floating-placeholder">
										<input type="text" id="${ secondParam }" value="" />	
										<label for="${ secondParam }"> ${ secondParamValue } </label>
									</div>

									<button class="save button--state-save" type="button"> Save </button>
									<button class="cancel button--state-cancel" type="button"> Cancel </button>
								</div>
							</div>
						</div>
				`;

				userInfoTemplateContainer.appendChild(userInfoTemplate);
				
			}

			let isEditValid = true;

			let templateHolder = document.getElementById('about-section');
			
			if(templateHolder != null) {
				templateHolder.appendChild(containerHolder);
				templateHolder.appendChild(userInfoTemplateContainer);

			}

			let editButton = document.querySelectorAll('.edit'),
					saveButton = document.querySelectorAll('.save'),
					cancelButton = document.querySelectorAll('.cancel');

			_self.setHeaderInfo();

			for(var editBtn = 0 ; editBtn < editButton.length ; editBtn++) {
				editButton[editBtn].addEventListener('click' , (e) => {
					let _this = e.currentTarget;
					let activePopUp = document.querySelector('.person-info.active');

					if(activePopUp != null) {
						activePopUp .classList.remove('active');
					}

					_this.parentElement.nextElementSibling.firstElementChild.firstElementChild.value = '';
					_this.parentElement.nextElementSibling.firstElementChild.classList.remove('input-filled');
					_this.parentNode.parentNode.classList.add('active');
				});
			}

			for(var saveBtn = 0 ; saveBtn < saveButton.length ; saveBtn++) {
				saveButton[saveBtn].addEventListener('click' , (e) => {
					let _this = e.currentTarget;
					let propName = _this.parentElement.parentElement.getAttribute('data-prop');
					let newPropValue = _this.previousElementSibling.firstElementChild.value;
					let propNameTitle = document.querySelector(`[data-prop="${propName}"] .default-info span`);

					if(newPropValue != '' && newPropValue.length > 0) {
						_self.setHeaderInfoOnUpdate(propName, newPropValue);

						for(let key in person.data) {	
							let firstParam = Object.keys(person.data[key])[0];	

							if(firstParam == propName) {
								person.data[key][propName] = newPropValue;
							} 
						}

						_self.updateUserInfo();
						propNameTitle.innerHTML = newPropValue;					
						_this.parentNode.parentNode.classList.remove('active');
					} else {
						alert('Field cannot be empty');
					}
				});
			}

			for(var cancelBtn = 0 ; cancelBtn < cancelButton.length ; cancelBtn++) {
				cancelButton[cancelBtn].addEventListener('click' , (e) => {
					let _this = e.currentTarget;

					if(_this.parentNode.parentNode.classList.contains('active')) {
						_this.parentNode.parentNode.classList.remove('active');
					}
				});
			}

			//Chechking the input field if it's empty or not.

			let floatingPlaceholderContainer = document.querySelectorAll('.floating-placeholder input');

			
				for(let el = 0 ; el < floatingPlaceholderContainer.length ; el++) {
					floatingPlaceholderContainer[el].addEventListener('keyup' , _self.checkInput);

					window.addEventListener('resize' , () => {
						floatingPlaceholderContainer[el].addEventListener('keyup' , _self.checkInput);
					});
				}
			
		}

		this.checkInput = (e) => {
			let _this = e.currentTarget;

			_self.triggerClickOnKeyup(e , _this);

			let isInputFilled = _this.value.length ? _this.parentElement.classList.add('input-filled') : _this.parentElement.classList.remove('input-filled');			
		}

		this.triggerClickOnKeyup = (e , _this) => {
			if(e.keyCode == 13) {
				if(window.innerWidth < 768) {
					let mobileSaveBtn = document.querySelector('.mobile--save');

					mobileSaveBtn.click();	
				} else {
					_this.parentElement.nextElementSibling.click();
				}	
			}

			if(e.keyCode == 27) {
				if(window.innerWidth < 768) {
					let mobileCancelBtn = document.querySelector('.mobile--cancel');

					mobileCancelBtn.click();	
				} else {
					_this.parentElement.nextElementSibling.nextElementSibling.click();
				}	
			}
		}
	};

window.onload = () => {
	AT_CH = new AT_CH_CONTROLLER();
	AT_CH.init();
}